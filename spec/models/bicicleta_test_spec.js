var Bicicleta = require('../../models/bicicleta');

beforeEach(
    ()=> {
        Bicicleta.allBicis = [];
    }
);

describe('Bicicleta.allBicis', () => {
    it('comienza vacía',() => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una bicicleta',() => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [-25.273805, -57.546322]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById',() => {
    it('Debe devolver la Bici con id 1',() => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1,"verde","mountain");
        var aBici2 =  new Bicicleta(2,"roja","urbana");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);


    });
});

describe ('removeById', ()=> {
    it('Agrego2 bicis, quito 1, debe devolver 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1,"verde","mountain");
        var aBici2 =  new Bicicleta(2,"roja","urbana");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
});