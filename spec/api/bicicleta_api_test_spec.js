var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicletas API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-25.273805, -57.546322]);
            Bicicleta.add(a);

            request.get('http://127.0.0.1:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });


            expect(Bicicleta.allBicis.length).toBe(1);

        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type':'application/json'};
            var aBici = '{"id":10, "color": "rojo","modelo":"urbana","lat": -34, "lng":-54}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });

   describe('POST BICICLETAS /delete', ()=> {
        it ('DELETE Bici 2', (done)=>{
             expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo'  , 'urbana', [-25.273805, -57.546322]);
            var b = new Bicicleta(2, 'blanco', 'urbana', [-25.272291, -57.541344]);
 
            Bicicleta.add(a);
            Bicicleta.add(b);
  
            var headers = {'content-type':'application/json'};
            var todelete = '{"id":2}';

            request.delete({
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: todelete
            }, function(error, response, body){
                expect(response.statusCode).toBe(204);
                done();             
            });       
        });
    });

    describe ('UPDATE Bicicletas /update', ()=> {
        it('Status 200 - Update', (done)=>{

            var a = new Bicicleta(1, 'rojo'  , 'urbana', [-25.273805, -57.546322]);
            var b = new Bicicleta(2, 'blanco', 'urbana', [-25.272291, -57.541344]);
 
            Bicicleta.add(a);
            Bicicleta.add(b);            

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":2, "color": "verde","modelo":"carrera","lat": -34, "lng":-54}';
           request.post({
               headers : headers,
               url: 'http://localhost:3000/api/bicicletas/update',
               body: aBici
           }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(2).color).toBe('verde');
            expect(Bicicleta.findById(2).modelo).toBe('carrera');
            done();
           });
        });
    });

});